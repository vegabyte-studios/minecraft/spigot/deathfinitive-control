package studio.vegabyte.minecraft.spigot.deathfinitive_control.managers;

import java.util.List;

import com.github.jikoo.experience.Experience;

import org.bukkit.GameRule;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import studio.vegabyte.minecraft.spigot.deathfinitive_control.App;
import studio.vegabyte.minecraft.spigot.deathfinitive_control.Outcome;
import studio.vegabyte.minecraft.spigot.deathfinitive_control.items.DeathCompassItem;
import studio.vegabyte.minecraft.spigot.deathfinitive_control.services.ConfigurationService;

public class PlayerDeathManager implements Listener {
  // #region constants

  /**
   * Order to iterate the invntory in. Indices of armor are last, so the new
   * indices of unequipped armors aren't reiterated.
   * 
   * Necessary to iterate off-hand before equipment slots. So if equipment is
   * moved to off-hand, then it is not reiterated.
   */
  private int[] iterationOrder = new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
      21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 40, 36, 37, 38, 39 };

  // #endregion

  // #region handlers

  /**
   * Update experience for lose, drop and keep for a death event.
   * 
   * @param calculateDroppedExperience Manually set new exp and manuall calculate
   *                                   dropped exp.
   */
  private void updateExperience(PlayerDeathEvent playerDeathEvent, boolean setNewExperienceImmediately) {
    playerDeathEvent.setKeepLevel(false); // override manual level setting

    // determine sum of involved experience
    int experienceTotal = 0;
    String experienceTotalConfig = this.configurationService.GetExperienceTotal();
    if (experienceTotalConfig.equals("vanilla")) {
      // total up to vanila
      experienceTotal = Math.min((int) 7 * playerDeathEvent.getEntity().getLevel(), 100);
    } else {
      // try to parse experienceTotal as ratio
      try {
        experienceTotal = (int) Math
            .ceil(Experience.getExp(playerDeathEvent.getEntity()) * Double.valueOf(experienceTotalConfig));
        // current
      } catch (NumberFormatException ex) {
        this.app.getLogger()
            .info(String.format("Unable to parse value of experienceTotal: '%s'", experienceTotalConfig));
      }
    }

    // determine experience to keep
    // default up to vanilla, but not more than experienceTotal
    int experienceKeep = 0;
    String experienceKeepFormula = this.configurationService.GetExperienceKeep();
    if (experienceKeepFormula.equals("vanilla")) {
      // keep up to vanilla, but no more than total
      experienceKeep = Math.min(Math.min((int) 7 * playerDeathEvent.getEntity().getLevel(), 100), experienceTotal);
    } else {
      // try to parse experienceKeep as ratio
      try {
        experienceKeep = (int) Math.round(experienceTotal * Double.valueOf(experienceKeepFormula));
      } catch (NumberFormatException ex) {
        this.app.getLogger()
            .info(String.format("Unable to parse value of experienceKeep: '%s'", experienceKeepFormula));
      }
    }

    // determine experience to lose
    int experienceLose = experienceTotal - experienceKeep;

    // determine experience to drop
    int experienceDrop = Math.round(experienceLose * (float) this.configurationService.GetExperienceDropRatio());

    // set experience to keep
    if (setNewExperienceImmediately) {
      Experience.setExp(playerDeathEvent.getEntity(), experienceKeep); // ~ set directly, not playerDeathEvent
    } else {
      playerDeathEvent.setNewExp(experienceKeep);
    }

    // set experience to drop
    playerDeathEvent.setDroppedExp(experienceDrop);

    // this.app.getLogger().info(String.format("experience: total %d, lose %d, keep
    // %d, drop %d.", experienceTotal,
    // experienceLose, experienceTotal, experienceDrop));
  }

  /**
   * Update inventory drops for a death event. Determines item outcomes via
   * configuration.
   * 
   * @param playerDeathEvent
   */
  private void updateDropsAndInventory(PlayerDeathEvent playerDeathEvent) {
    PlayerInventory inventory = playerDeathEvent.getEntity().getInventory();

    // get configuration
    Outcome[] configKeepSlots = this.configurationService.GetInventorySlotsAsOutcome();
    String configCurseOfBindingMode = this.configurationService.GetInventoryCurseOfBindingMode();
    String configCurseOfVanishingMode = this.configurationService.GetInventoryCurseOfVanishingMode();

    // clear out drops, for manual determination
    List<ItemStack> drops = playerDeathEvent.getDrops();
    drops.clear();

    // keep items on death (without restoring on spawn)
    playerDeathEvent.setKeepInventory(true);

    for (int i = 0; i < 41; i++) { // highest index of 40 as of 1.16
      int iSlot = iterationOrder[i];

      ItemStack iItem = inventory.getItem(iSlot);

      // do nothing with null items
      if (iItem == null)
        continue;

      // this.app.getLogger().info(String.format("evaluating item. item: %d,
      // configBinding: %s, configVanish: %s", i,
      // configCurseOfBindingMode, configCurseOfVanishingMode));

      boolean iDrop = false;
      boolean iVanish = false;

      // handle slots
      iVanish = configKeepSlots[i] == Outcome.VANISH;
      iDrop |= configKeepSlots[i] == Outcome.DROP;

      // this.app.getLogger().info(String.format("evaluated slots. drop: %s, vanish:
      // %s.", iDrop, iVanish));

      // handle curse of vanishing
      boolean cursedVanish = iItem.getEnchantmentLevel(Enchantment.VANISHING_CURSE) > 0;
      iVanish |= cursedVanish && configCurseOfVanishingMode.equals("vanish");
      iDrop |= cursedVanish && configCurseOfVanishingMode.equals("drop");

      // this.app.getLogger().info(String.format("evaluated curse of vanishing. drop:
      // %s, vanish: %s.", iDrop, iVanish));

      // handle curse of binding
      // ~ handle binding *LAST* because it could potentially move
      // ~ check if already dropping or vanishing; cannot move a lost object
      boolean cursedBinding = iItem.getEnchantmentLevel(Enchantment.BINDING_CURSE) > 0;
      if (!iDrop && !iVanish && cursedBinding && iSlot >= 36 && iSlot <= 39) {
        // handle any equipped items with curse of binding in "unequip" mode
        if (configCurseOfBindingMode.contains("keep-unequip")) {

          int firstEmptySlot = inventory.firstEmpty(); // does *NOT* consider equipment slots NOR off hand

          // check additional slots which firstEmpty does not consider
          // ~ as of 1.16.3, the only slot that is not considered by firstEmpty is the
          // off-hand, index 40
          if (firstEmptySlot == -1 || firstEmptySlot > 35 && firstEmptySlot < 40 && inventory.getItem(40) == null) {
            firstEmptySlot = 40;
          }

          if (firstEmptySlot > -1) {
            // move to first empty slot
            inventory.setItem(iSlot, null); // delete
            inventory.setItem(firstEmptySlot, iItem); // add

            // ~ the index has changed; this is fine.
            // equipment is last in the iteration order, and equipment cannot be moved into
            // equipment slots
          } else {
            // no empty slots. use backup

            // ~ didn't move, can keep index
            iDrop |= configCurseOfBindingMode.equals("keep-unequip-drop");
            iVanish |= configCurseOfBindingMode.equals("keep-unequip-vanish");
          }
        } else { // not keep-unequip; keep, drop, vanish
          iDrop |= configCurseOfBindingMode.equals("drop");
          iVanish |= configCurseOfBindingMode.equals("vanish");
        }
      }

      // this.app.getLogger().info(String.format("evaluated curse of binding. drop:
      // %s, vanish: %s.", iDrop, iVanish));

      // lost, remove from inventory
      if (iDrop || iVanish) {
        // ~ can't move and lose an item
        inventory.setItem(iSlot, null); // lost, clear the inventory slot
      }

      // add drops to drops
      if (iDrop && !iVanish) {
        drops.add(iItem);
      }
    }
  }

  /**
   * Add a {@link DeathCompassItem} to the player's inventory.
   */
  private void updateInventoryAddDeathCompassItem(PlayerDeathEvent playerDeathEvent) {
    PlayerInventory inventory = playerDeathEvent.getEntity().getInventory();

    int firstEmptySlot = inventory.firstEmpty(); // does *NOT* consider equipment slots NOR off hand

    // check additional slots which firstEmpty does not consider
    // ~ as of 1.16.3, the only slot that is not considered by firstEmpty is the
    // off-hand, index 40
    if (firstEmptySlot == -1 || firstEmptySlot > 35 && firstEmptySlot < 40 && inventory.getItem(40) == null) {
      firstEmptySlot = 40;
    }

    if (firstEmptySlot == -1) // when no empty slots
      return; // do nothing

    inventory.addItem(this.deathCompassItem.Instantiate(playerDeathEvent));
  }

  @EventHandler
  public void onPlayerDeath(PlayerDeathEvent playerDeathEvent) {

    String deathFinitiveKeepInventory = this.configurationService.GetDeathfinitiveKeepInventory();

    // determine the gamerule keepInventory
    boolean gameRuleKeepInventory = playerDeathEvent.getEntity().getWorld().getGameRuleValue(GameRule.KEEP_INVENTORY);

    // ignore if not enabled
    // NOT always
    // deathfinitiveKeepInventory and keepInventory mismatch
    if (!deathFinitiveKeepInventory.equals("always")
        && Boolean.valueOf(deathFinitiveKeepInventory) != gameRuleKeepInventory)
      return;

    // ignore if not in game mode
    if (!playerDeathEvent.getEntity().getGameMode().toString().matches(this.configurationService.GetGameModesManaged()))
      return;

    this.updateDropsAndInventory(playerDeathEvent);
    this.updateExperience(playerDeathEvent, gameRuleKeepInventory);

    // add compass
    if (this.configurationService.getCompassEnabled())
      this.updateInventoryAddDeathCompassItem(playerDeathEvent);
  }

  // #endregion

  // #region dependencies

  private App app;
  private ConfigurationService configurationService;
  private DeathCompassItem deathCompassItem;

  // #endregion

  // #region Lifecycle

  public PlayerDeathManager(App app, ConfigurationService configurationService, DeathCompassItem deathCompassItem) {
    this.app = app;
    this.configurationService = configurationService;
    this.deathCompassItem = deathCompassItem;

    this.app.getServer().getPluginManager().registerEvents(this, app);
  }

  // #endregion
}