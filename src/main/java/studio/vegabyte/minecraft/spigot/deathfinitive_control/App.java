package studio.vegabyte.minecraft.spigot.deathfinitive_control;

import org.bukkit.plugin.java.JavaPlugin;

import studio.vegabyte.minecraft.spigot.deathfinitive_control.items.DeathCompassItem;
import studio.vegabyte.minecraft.spigot.deathfinitive_control.managers.DeathCompassManager;
import studio.vegabyte.minecraft.spigot.deathfinitive_control.managers.PlayerDeathManager;
import studio.vegabyte.minecraft.spigot.deathfinitive_control.services.ConfigurationService;

public class App extends JavaPlugin {
  private ConfigurationService configurationService;
  private PlayerDeathManager playerDeathManager;
  private DeathCompassItem deathCompassItem;
  private DeathCompassManager deathCompassManager;

  @Override
  public void onEnable() {
    this.configurationService = new ConfigurationService(this);
    this.deathCompassItem = new DeathCompassItem(this, configurationService);
    this.playerDeathManager = new PlayerDeathManager(this, configurationService, this.deathCompassItem);
    this.deathCompassManager = new DeathCompassManager(this, this.deathCompassItem);
  }

  @Override
  public void onDisable() {

  }
}