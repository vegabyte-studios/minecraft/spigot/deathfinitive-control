package studio.vegabyte.minecraft.spigot.deathfinitive_control.services;

import java.util.Arrays;
import java.util.List;

import org.bukkit.GameMode;
import org.bukkit.configuration.file.FileConfiguration;

import studio.vegabyte.minecraft.spigot.deathfinitive_control.App;
import studio.vegabyte.minecraft.spigot.deathfinitive_control.Outcome;

// contains configuration
public class ConfigurationService {

  // #region variables

  private Outcome[] inventoryKeepSlotsAsOutcome;

  // #endregion

  // #region General

  public String GetDeathfinitiveKeepInventory() {
    return config.getString("deathfinitiveKeepInventory");
  }

  public String GetGameModesManaged() {
    return config.getString("gameModesManaged");
  }

  // #endregion

  // #region Methods - Inventory

  /**
   * Get inventory slot outcome as text.
   * 
   * @return
   */
  public List<String> GetInventorySlots() {
    return this.config.getStringList("inventorySlots");
  }

  /**
   * Get inventory slots as outcome as enum.
   * 
   * @return
   */
  public Outcome[] GetInventorySlotsAsOutcome() {
    return this.inventoryKeepSlotsAsOutcome;
  }

  /**
   * Get curse of binding mode.
   */
  public String GetInventoryCurseOfBindingMode() {
    return this.config.getString("inventoryCurseOfBindingOutcome");
  }

  /**
   * Get curse of binding mode.
   */
  public String GetInventoryCurseOfVanishingMode() {
    return this.config.getString("inventoryCurseOfVanishingOutcome");
  }

  // #endregion

  // #region Methods - Experience

  /**
   * Get which experience
   */
  public String GetExperienceTotal() {
    return this.config.getString("experienceTotal");
  }

  /**
   * Get the amount of total experience to lose from death. Scalar of current
   * experience.
   */
  public String GetExperienceKeep() {
    return this.config.getString("experienceKeep");
  }

  /**
   * Get ratio of lost experience to drop on death.
   */
  public double GetExperienceDropRatio() {
    return this.config.getDouble("experienceDropRatioOfLost");
  }

  // #endregion

  // #region Methods - Compass

  /**
   * Get if compass is enabled.
   */
  public boolean getCompassEnabled() {
    return this.config.getBoolean("compassEnabled");
  }

  // #endregion

  // #region Lifecycle

  App app;
  FileConfiguration config;

  public ConfigurationService(App app) {
    this.app = app;

    this.config = app.getConfig();

    // general
    config.addDefault("deathfinitiveKeepInventory", "always");
    config.addDefault("gameModesManaged", GameMode.SURVIVAL.toString() + "|" + GameMode.ADVENTURE.toString());
    // experience
    config.addDefault("experienceTotal", 1.0d);
    config.addDefault("experienceKeep", "vanilla");
    config.addDefault("experienceDropRatioOfLost", 1.0d);
    // inventory
    config.addDefault("inventoryCurseOfBindingOutcome", "keep-unequip");
    config.addDefault("inventoryCurseOfVanishingOutcome", "vanish");
    config.addDefault("inventorySlots", inventorySlotsDefault);
    // compass
    config.addDefault("compassEnabled", true);

    config.options().copyDefaults(true);

    this.app.saveConfig();

    // save inventory slot outcomes as enum
    List<String> slots = this.GetInventorySlots();
    this.inventoryKeepSlotsAsOutcome = new Outcome[slots.size()];
    for (int i = 0; i < inventoryKeepSlotsAsOutcome.length; i++) {

      this.inventoryKeepSlotsAsOutcome[i] = Outcome.valueOf(slots.get(i));
    }
  }

  // #endregion

  // #region constants

  private final List<String> inventorySlotsDefault = Arrays.asList("KEEP", "KEEP", "KEEP", "KEEP", "KEEP", "KEEP",
      "KEEP", "KEEP", "KEEP", "DROP", "DROP", "DROP", "DROP", "DROP", "DROP", "DROP", "DROP", "DROP", "DROP", "DROP",
      "DROP", "DROP", "DROP", "DROP", "DROP", "DROP", "DROP", "DROP", "DROP", "DROP", "DROP", "DROP", "DROP", "DROP",
      "DROP", "DROP", "KEEP", "KEEP", "KEEP", "KEEP", "KEEP");

  // #endregion
}
