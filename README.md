# **Overview**

Deathfinitive Control is the definitive death control plugin. Control item
slots, experience drops, and more!

# **Installation**

- download the .jar into your plugins folder
- it is **recommended** that you set the game rule `keepInventory` to `false`
  due to some "undefined" behaviors with experience in Spigot
- it is recommended that you configure the plugin before use

# **Concept - Outcomes**

There are 3 possible outcomes to items and experience on death:

- to `keep` means it remains in your inventory
- to `drop` means it leaves your inventory and appears in the world
- to `vanish` means it disappears forever

Additionally, to `lose` something means you didn't `keep` it. NB, to `lose` is
to `drop` or to `vanish`.

# **Module - Inventory**

The outcome of an inventory item is configurable:

- control outcome of specific inventory slots
  - eg, `keep` hotbar and equipment, `drop` everything else
- control outcome of cursed items
  - eg, `keep` (and unequip) Curse of Binding items
  - eg, `drop` Curse of Vanishing items

The Curse of Binding is only considered when the item is **equipped**.

## **Scenarios**

A higher priority always overrides a lower one: `keep` > `drop` > `vanish`

Here are some configuration scenarios to illustrate how outcome priority
functions (see **Configuration** for details)

- equipped armor with Curse of Vanishing
  - `inventorySlots`: configured to `keep`
  - `inventoryCurseOfVanishingOutcome`: configured to `drop`
  - outcome: `drop`
- equipped armor with Curse of Binding and Curse of Vanishing
  - `inventorySlots`: configured to `keep`
  - `inventoryCurseOfVanishingOutcome`: configured to `drop`
  - `inventoryCurseOfBindingOutcome`: configured to `vanish`
  - outcome: `vanish`
- equipped armor with Curse of Binding and Curse of Vanishing
  - `inventorySlots`: configured to `keep`
  - `inventoryCurseOfBindingOutcome`: configured to `keep`
  - `inventoryCurseOfVanishingOutcome`: configured to `drop`
  - outcome: `drop`

# **Module - Experience**

The experience involved in a death is configurable:

- control the total experience involved with a death
- control how much experience to `keep` (the rest is lost)
- control how much of the lost experience to `drop` (the rest vanishes)

## **Scenarios**

Here are some common configuration scenarios to illustrate how experience is
calculated (see **Configuration** for details)

- `keep` the amount of experience which usually drops in vanilla, `drop`
  remainder
  - `experienceTotal`: 1.0
  - `experienceKeep`: vanilla
  - `experienceDropRatioOfLost`: 1.0
- `keep` all experience, `drop` no experience
  - `experienceTotal`: 1.0
  - `experienceKeep`: 1.0
  - `experienceDropRatioOfLost`: 0.0
- `keep` no experience, `drop` all the experience
  - `experienceTotal`: 1.0
  - `experienceKeep`: 0
  - `experienceDropRatioOfLost`: 1.0
- `keep` no experience, `drop` the vanilla amount of experience
  - `experienceTotal`: vanilla
  - `experienceKeep`: 0
  - `experienceDropRatioOfLost`: 1.0

# **Configuration**

The default configuration makes death punishing, but not brutal:

- `keep` your hotbar, off-hand, and equipment
- `keep` and unequip **equipped** Curse of Binding equipment
- `drop` Curse of Vanishing items
- `keep` as much as a vanilla exp drop, and `drop` the remainder

The layout of each configuration follows this pattern<br>
`configuration name: [setting 1 | setting 2 | setting 3 ...] = default value`

## **General**

### **deathfinitiveKeepInventory**: [always | true | false] = always

Determines when Deathfinitive should control outcomes

- `always`: Deathfinitive always manages deaths
- `true`: Deathfinitve manages deaths when game rule `keepInventory` is `true`
- `false`: Deathfinitive manages deaths when game rule `keepInventory` is
  `false`

### **gameModesManaged**: [SURVIVAL | ADVENTURE | CREATIVE] = SURVIVAL|ADVENTURE

Regex of GameMode which Deathfinitive manages.

List of possible values:

- `SURVIVAL`
- `ADVENTURE`
- `CREATIVE`
- `SPECTATOR`

## **Experience**

### **experienceTotal**: [vanilla | 0 - 1.0] = 1.0

The sum of all experience to `keep` and `drop`. ie, the experience that does not
`vanish`. Either `vanilla`, or a `ratio`.

- `vanilla`: (total experience) = 7 * (current level)
- `ratio`: (total experience) = (current experience) * `experienceTotal`
  - eg, `experienceTotal`: 0.5 will have half of the player's experience to drop

### **experienceKeep**: [vanilla | 0 - 1.0] = vanilla

The formula used to determine how much experience to `keep`. ie, Of the total
experience, how much of it to `keep`?

Either `vanilla`, or a `ratio`.

- `vanilla`: (kept experience) <= 7 * (current level) * 100
- `ratio`: (kept experience) = (total experience) * `experienceKeep`

### **experienceDropRatioOfLost**: [0 - 1.0] = 1.0

Ratio of 'lost' experience to `drop`. All remaining experience will `vanish`.

- ie, of the _total experience_ not to `keep`, how much should `drop`?
- eg, 1.0: all of the lost experience will `drop`
- eg, 0.5: half of the lost experience will `drop`

(dropped experience) = ((total experience) - (kept experience)) *
`experienceDropRatioOfLost`

## **Inventory**

### **inventoryCurseOfVanishingOutcome**: [keep | vanish | drop] = drop

How to determine the outcome of items with Curse of Vanishing.

- `keep`: item is kept
- `drop`: item will drop
- `vanish`: item will vanish

### **inventoryCurseOfBindingOutcome**: [keep | keep-unequip | keep-unequip-drop | keep-unequip-vanish | drop | vanish] = keep-unequip

How to determine the outcome of **equipped** items with Curse of Binding.

- `keep`: Does nothing. ie, stays in the equipment slot.
- `keep-unequip`: moved to first available inventory slot. If no slot available,
  does nothing.
- `keep-unequip-drop`: moved to first available inventory slot. If no slot
  available, drops.
- `keep-unequip-vanish`: moved to first available inventory slot. If not slot
  available, vanishes.
- `drop`: item will `drop`.
- `vanish`: item will `vanish`.

After being unequipped, items will not be affected by setting of
`inventorySlots` they go into. eg, if an item is `keep-unequip` and moved into a
slot which is configured to `vanish`. the unequipped item will _not_ `vanish`.

### **inventorySlots**: array of [KEEP | DROP | VANISH]

An array of each slot's outcome, by index

- `KEEP`: the item is kept
- `DROP`: the item is dropped
- `VANISH`: the item vanishes

Default preset:

- hotbar: 0-8 (default `keep`)
- inventory: 9-35 (default `drop`)
- equipment: 36-39 (default `keep`)
- off hand: 40 (default `keep`)

## Compass

### **compassEnabled** : boolean = true

Should a compass which points to the location of a death be given to the player
after death?

# **Bugs and Limitations**

## **New experience is immediately set on death when game rule `keepInventory` is `true`**

There is currently an undefined behavior in Spigot for player death events when
the game rule _keepInventory_ is _true_. The workaround implemented in this
plugin is to immediately set the experience to `keep` after death, instead of
changing after respawn. This workaround does not affect gameplay.
